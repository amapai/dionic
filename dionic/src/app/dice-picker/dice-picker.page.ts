import { Component, OnInit } from '@angular/core';
import * as diceGlobals from '../dice';

@Component({
  selector: 'app-dice-picker',
  templateUrl: './dice-picker.page.html',
  styleUrls: ['./dice-picker.page.scss'],
})
export class DicePickerPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public setDiceSize(nb : number) {
	  console.log('new size : '+nb);
	  diceGlobals.globalDice.setNewDiceMaxVal(nb);
  }
}
