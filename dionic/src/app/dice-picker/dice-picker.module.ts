import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DicePickerPageRoutingModule } from './dice-picker-routing.module';

import { DicePickerPage } from './dice-picker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DicePickerPageRoutingModule
  ],
  declarations: [DicePickerPage]
})
export class DicePickerPageModule {}
