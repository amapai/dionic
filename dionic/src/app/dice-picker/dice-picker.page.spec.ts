import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DicePickerPage } from './dice-picker.page';

describe('DicePickerPage', () => {
  let component: DicePickerPage;
  let fixture: ComponentFixture<DicePickerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicePickerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DicePickerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
