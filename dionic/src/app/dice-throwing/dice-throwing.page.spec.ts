import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiceThrowingPage } from './dice-throwing.page';

describe('DiceThrowingPage', () => {
  let component: DiceThrowingPage;
  let fixture: ComponentFixture<DiceThrowingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiceThrowingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiceThrowingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
