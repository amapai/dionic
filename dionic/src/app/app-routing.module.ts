import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'dice-throwing',
    loadChildren: () => import('./dice-throwing/dice-throwing.module').then( m => m.DiceThrowingPageModule)
  },
  {
    path: 'dice-picker',
    loadChildren: () => import('./dice-picker/dice-picker.module').then( m => m.DicePickerPageModule)
  },
  {
    path: 'dice-history',
    loadChildren: () => import('./dice-history/dice-history.module').then( m => m.DiceHistoryPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
