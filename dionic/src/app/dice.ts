export class Dice {
	maxValueToRoll : number;
	historyDice: [number, number][];

	public constructor() {
		this.maxValueToRoll = 6;
		this.historyDice = [];
	}

	public getRandomValueFromDice() {
		var diceRoll = Math.floor(Math.random() * Math.floor(this.maxValueToRoll))+1;
		// Get a random value, store it in history and return it
		this.historyDice.unshift([diceRoll, this.maxValueToRoll]);
		return diceRoll;
	}

	public setNewDiceMaxVal(newVal: number) {
		this.maxValueToRoll = newVal;
	}
}

export let globalDice: Dice = new Dice();
